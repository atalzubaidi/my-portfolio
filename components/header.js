import Link from 'next/link';
import {useEffect} from 'react'
function Header({page}){
    var previusPage = page;
    useEffect(() => {
        // 👇 add class to body element
        document.getElementById(page).classList.add('selected');
    
    })

    return(
        <div className='header bg-layout shadow-md flex justify-between fixed z-[99] w-full top-0'>
            <div className="h-full w-2/6 pl-12 flex items-center">LOGO</div>
            <div className="h-full w-4/6 flex justify-around items-center">
                <Link href='/'><h1 id='nav-home' className='cursor-pointer text-xl'>Home</h1></Link>
                <Link href='/about'><h1 id='nav-about' className='cursor-pointer text-xl'>About</h1></Link>
                <Link href='/my-work'><h1 id='nav-work' className='cursor-pointer text-xl'>My Work</h1></Link>
            </div>
        </div>
    )
}

export default Header