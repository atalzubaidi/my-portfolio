import Link from "next/link"
export default function WorkNav(){
    return(
        <div className="fixed top-[300px] right-6">
            <div className="flex flex-col">
                <div className="flex flex-row-reverse gap-2">
                    <Link href='/graphic-design'>
                    <div className="social group peer hover:bg-[#00a0ff]">
                        <img src="/computer.png" className="social-content group-hover:invert" />
                    </div>
                    </Link>
                    <p className="bg-[#00a0ff] bg-opacity-60 h-min rounded-xl p-1 text-white font-bold invisible peer-hover:visible">Graphic Design</p>
                </div>

                <div className="flex flex-row-reverse gap-2">
                    <Link href='/portrait'>
                    <div className="social group peer hover:bg-[#00a0ff]">
                        <img src="/image.png" className="social-content group-hover:invert" />
                    </div>
                    </Link>
                    <p className="bg-[#00a0ff] bg-opacity-60 h-min rounded-xl p-1 text-white font-bold invisible peer-hover:visible">Portrait Art</p>
                </div>
                
                <div className="flex flex-row-reverse gap-2">
                    <Link href='/concept'>
                    <div className="social group peer hover:bg-[#00a0ff]">
                        <img src="/cityscape.png" className="social-content group-hover:invert" />
                    </div>
                    </Link>
                    <p className="bg-[#00a0ff] bg-opacity-60 h-min rounded-xl p-1 text-white font-bold invisible peer-hover:visible">Concept Art</p>
                </div>

                <div className="flex flex-row-reverse gap-2">
                    <Link href='/motion'>
                    <div className="social group peer hover:bg-[#00a0ff]">
                        <img src="/play.png" className="social-content group-hover:invert" />
                    </div>
                    </Link>
                    <p className="bg-[#00a0ff] bg-opacity-60 h-min rounded-xl p-1 text-white font-bold invisible peer-hover:visible">Motion Graphics</p>
                </div>

            </div>
        </div>
    )
}