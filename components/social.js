export default function Social(){
    return(
        <div className="fixed top-[100px] right-6">
            <div className="flex flex-col">
                <div className="social group hover:bg-[#00a0ff]"><img src="/twitter.png" className="social-content group-hover:invert" /></div>
                <div className="social group hover:bg-[#00a0ff]"><img src="/linkedin.png" className="social-content group-hover:invert" /></div>
                <div className="social group hover:bg-[#00a0ff]"><img src="/instagram.png" className="social-content group-hover:invert" /></div>
            </div>
        </div>
    )
}