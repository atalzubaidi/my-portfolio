module.exports = {
  content: ["./pages/**/*.{html,js}", , "./components/**/*.{html,js}"],
  theme: {
    extend: {
      colors: {
        'primary': '#00a0ff',
        'layout': '#303030'
      }
    },
  },
  plugins: [],
}
