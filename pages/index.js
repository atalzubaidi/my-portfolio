import Head from 'next/head'
import Link from 'next/link'
import Header from '../components/header'
import Social from '../components/social'


export default function Home() {
  return (
    <>
    <div className='h-[100vh] bg-layout overflow-hidden z-0'>
      <Head>
        <title>My Portfolio</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header page={'nav-home'}></Header>
      <Social></Social>
      <div className='h-full flex flex-col justify-center items-center'>
        <h1 className='z-30 text-5xl font-bold text-primary text-shadow'>Ammar Alzubaidi</h1>
        <h2 className='z-30 text-2xl text-white mt-5'>Web Developer</h2>
        <Link href='/about'><button className='bg-primary z-30 h-[60px] w-[200px] text-white text-lg font-bold p-3 rounded-full mt-12 hover:bg-white hover:text-black duration-200'>About Me</button></Link>
      </div>
    </div>
    <div className='fixed bg-primary bg-opacity-20 h-[400px] w-full skew-y-12 z-10 top-[300px] left-0'>
    </div>
    </>
  )
}
