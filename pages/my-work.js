import Head from 'next/head'
import Header from '../components/header'
import Social from '../components/social'
import WorkNav from '../components/workNav';
import Link from 'next/link';


export default function Work() {


  return (
    <div className='page h-[100%] min-h-[100vh] pb-12 bg-[#303030]'>
      <Head>
        <title>My Portfolio</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header page={'nav-work'}></Header>
      <Social></Social>
      <div className='flex flex-col items-center my-6'>
        <h1 className='text-5xl font-bold text-[#00a0ff] text-shadow'>My Work</h1>
      </div>
      <div className='h-[80%] min-w-[500px] bg-blue-100 bg-opacity-10 rounded-3xl p-10 ml-14 mr-[90px]'>
      <Link href='/graphic-design'>
        <div className='work-card'>Graphic Design</div>
      </Link>
      <Link href='/portrait'>
        <div className='work-card'>Portrait Art</div>
      </Link>
      <Link href='/concept'>
        <div className='work-card'>Concept Art</div>
      </Link>
      <Link href='/motion'>
        <div className='work-card'>Motion Graphics</div>
      </Link>
      </div>
    </div>
  )
}
